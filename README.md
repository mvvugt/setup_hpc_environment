# Customize your HPC environment

I modified this file

The following tutorial explains how to configure your `bash` environment, when
working from a UNIX high performance computer cluster. 

Topics that will be covered: 

1. Configure your `.bashrc` - note, this is a hidden file (files with a '.' 
prefix will remain hidden unless you run `ls -a`).
2. Creating a custom directory for (shared) resources - specifically focussing
on situations were your allocated home directory is too small.
3. Installing software without admin (i.e., sudo) privileges. 
4. Working with 'conda' to manage python and R modules. 
5. A brief outline on git and how to use it from the command line. 

## Set-up your `.bashrc`

A `.bashrc` is a shell script that will run every time you start an 
interactive session. 
That is every time you open a terminal, for example by typing Ctrl+Alt+T.

A bashrc file used to configure your environment, setting specific options
or adding programs/paths. 
Both your cluster home directory, as well as your local machine will have a
`.bashrc`, or similar depending on the type of shell used
(e.g., mac will often come with `zsh` pre-installed). 
To view your (local or HPC) `.bashrc` run `less ~/.bashrc`. 

_Explanation:_
`less` is an interactive file viewer, and `~` is shorthand for 
your home director (try running `echo $HOME`).

Typically your .bashrc will be pretty empty at this point. 
Let's try to add a few helpful configurations. 

### History 

You can search your command line history by running `Ctrl+R` and `Ctrl+P`. 
The history is initially limited to the first few hundred commands.
This can be extended by adding the following to your `.bashrc`. 

```sh
# open `.bashrc`
nano ~/.bashrc
# or use vim: vim ~/.bashrc

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# History settings 
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# increasing history size and length
HISTSIZE=1000000
HISTFILESIZE=1000000
# alternativly, you could get unlimited history size and length by setting
# these to -1

```

Other relevant history settings to consider adding: 

```sh
   
# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend
```

_Explanation:_ 
After closing your bashrc, type `source ~/.bashrc` to reload
the files and make sure it all works.

### Shell prompt

Looking at your command line, where you type command to bash, there will 
be some minimal information on the LHS, the 'prompt'. 
For example: `username@hostname:~$ `. 

You can configure the prompt by specifying the `$PS1` environmental variable. 
For example we might want the prompt to return the _time_, _username_, _host_,
_directory_, _gitbranch_, ending with a `$`.

_Note_: see below for `__git_ps1`.

```sh
# check if the we can use colours
# if $TERM is equal to `xterm_color or *-256colour, set color_prompt to yes
case "${TERM}" in
    xterm-color|*-256color)
        color_prompt=yes
        ;;
esac

# configure PS1 and use a default if no colours
if [ "$color_prompt" == yes ]; then
    GREEN="$(tput setaf 2)"
    BLUE="$(tput setaf 4)"
    RED="$(tput setaf 1)"
    WHITE="$(tput setaf 7)"
    RESET="$(tput setf 7)"
    PS1='\[$GREEN}\][\A \u@\h]\[${WHITE}\]: \[${BLUE}\]\w\[${RED}\]$(__git_ps1 " (%s)")\[${WHITE\] \$ '
else
    # u = username, A = current time, h=hostname, w=current working directing, \\=escape
    PS1="[\A \u@\h]: \w $"
fi
# clean-up
unset color_prompt force_color_prompt
```

_Explanation:_ 
Have a look [here](http://faculty.salina.k-state.edu/tim/unix_sg/bash/ps1.html)
 for additional information on the meaning of the PS1 symbols.
`tput` might not be available in all environments, in that case used: 

```sh
PS1='\[\033[01;32m\][\A \u@\h]\[\033[00m\]\]\]: \[\033[01;34m\]\]\w\e[031m\]$(__git_ps1 " (%s)")\e[m\[\033[00m\]\] \$ '
```

### Miscellaneous functions

Some of the following functions might be useful to add to your `.bashrc`

```sh

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Miscellaneous functions
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

##### Better mkdir
# will make the directory 
# and cd into it 
mkcdir ()
{
    mkdir -p -- "$1" &&
      cd -P -- "$1"
}

# extract the header of a file
# alias header="head -n1 | tr $'\t' '\n' | cat -n"
# print the header, adding CR after each entry
# file : argument 1
# delimiter : argument 2
header() {
    delim=$2:-"\t"
    # the actual command
    head -n1 $1 | tr "$delim" "\n" | cat -n
}

# extract the body of a file
body() {
    IFS= read -r header
    printf '%s\n' "$header"
    "$@"
}

```

## Accessing a cluster

As an example we will focus on UCL's myriad [cluster]
(https://www.rc.ucl.ac.uk/docs/Clusters/Myriad/).

### Requesting access to Myriad

Before being able to use the cluster, you will need to request account access. 
The application form can be found [here](https://signup.rc.ucl.ac.uk/computing/requests/new).
For the Myriad pipeline, selecting `Individual single core jobs` as a resource requirement should suffice.

### SSHing to the cluster

Once you have account access to Myriad, you should be able to SSH into the 
machine by using the following command: 
```sh
ssh <your_UCL_user_id>@myriad.rc.ucl.ac.uk
```
If you are logging in from outside the UCL firewall, you will need to go through UCL's Socrates gateway:
```sh
ssh <your_UCL_user_id>@socrates.ucl.ac.uk
```
after which you should be able to ssh into Myriad as above.

To make this process easier, you can edit your ssh config on your local machine
(usually found at `~/.ssh/config`) by adding the following lines:

```sh
# jump to scorates
Host uclsocjump
    Hostname socrates.ucl.ac.uk
    User <your_UCL_user_id>
    IdentityFile ~/.ssh/myriad

# Myriad
Host myriad
User <your_UCL_user_id>
    HostName myriad.rc.ucl.ac.uk
    proxyCommand ssh -W %h:%p uclsocjump
    IdentityFile ~/.ssh/myriad

# Myriad without socrates
Host myriad_nojump
    User <your_UCL_user_id>
    HostName myriad.rc.ucl.ac.uk
    IdentityFile ~/.ssh/myriad

# Individual nodes
Host myriad_l12
    Hostname login12.myriad.rc.ucl.ac.uk
    User <your_UCL_user_id>
    ProxyCommand ssh -W %h:%p uclsocjump
    IdentityFile ~/.ssh/myriad

Host myriad_l13
    Hostname login13.myriad.rc.ucl.ac.uk
    User <your_UCL_user_id>
    ProxyCommand ssh -W %h:%p uclsocjump
    IdentityFile ~/.ssh/myriad
```
Note the `IdentityFile` path refers to your private ssh key.
These can be generated using the following code, which will ask your 
for an optional passphrase.
Myriad will not accept keys unless they are protected through a passphrase! 

```sh
# ### generating a ssh-key
cd ~/.ssh # create dir if needed
ssh-keygen -f ./myriad

# ### copy the public key to the remote
ssh-copy-id -i ./myriad.pub myriad
```

You can optionally install `keychain` which record your passphrase for a single session.

```sh
# ### install keychain 
# check if  it is installed
keychain --version
# if not install
sudo apt install keychain # linux
brew install keychain # MacOS

# add the following to your .bashrc 
vim ~/.bashrc
/usr/bin/keychain $HOME/.ssh/myriad 2> /dev/null
source $HOME/.keychain/$HOSTNAME-sh

# change your ~/.ssh/config by adding `AddKeysToAgent yes`
Host myriad
    User rmgpafs
    HostName myriad.rc.ucl.ac.uk
    proxyCommand ssh -W %h:%p uclsocjump
    IdentityFile ~/.ssh/myriad
```

You should now simply be able to run `ssh myriad` from your terminal to log 
into the cluster.
_Note_ do not forget to copy your public key to `~/.ssh/` on myriad, for 
example `myriad.pub`.

## Setting up git

git a great tool for version control, allowing you to track changes in your 
project. 
Typically `git` will be pre-installed on your computer and HPC. 
[This](https://git-scm.com/book/en/v2) book is a great starting point. 

Git can interface with online resources such as `gitlab` or `github`.
To repeatedly `push` commits to these online resources you will want to
generate a SSH key. 
You can do this by using the command

```sh
cd ~/.ssh && ssh-keygen -f <FILENAME>
```

Now add the public key to your account, following the provided 
instructions at [this link](https://docs.gitlab.com/ee/user/ssh.html). 
Please add the following to `~/.ssh/config`

```sh
# GitLab.com
Host gitlab.com
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/<FILENAME>
```

It may also be useful to configure `git` by using the following commands:

```sh
git config --global user.name "your_GitLab_username"
git config --global user.email "your_GitLab_email_address"
```

This will associate any commits that you make to you as a user.

Finally, to check you `ssh` connection work, run the following lines which 
should return a welcome message from GitLab:

```bash
ssh -T git@gitlab.com
```

### git settings

The following steps can improve your git workflow. 
First, download the following files to your home directory:

```sh
cd ~
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash

# make into hidden files
mv ~/git-prompt.sh ~/.git-prompt.sh
mv ~/git-completion.bash ~/.git-completion.bash
```

_Explanation:_ 
`.git-completion.sh` allows you to autocomplete git commands by (repeatedly) 
pressing `tab`, and `.git-prompt.sh` can be used to update your shell prompt
to indicate the current branch of your project.

We will discuss the prompt soon, but first let's add these two files to 
your `.bashrc`.

```sh
# open `.bashrc`
nano ~/.bashrc
# or use vim: vim ~/.bashrc

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# GIT
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# define completion for Git
# should check if present, if not wget to home
gitcomp="${HOME}/.git-completion.bash"
if [ -r "$gitcomp" ];then
    source $gitcomp
fi

# shell prompt for PS1
# should check if present, if not wget to home
ps_test="${HOME}/.git-prompt.sh"
if [ -r "$ps_test" ]; then
    source "$ps_test"
else
    # set to null otherwise
    echo ".git-prompt.sh unavailable"
    __git_ps1=
fi
```

_Explanation:_ 
the `[ ]` opperator should be understood as a test, which can either be `True`
or `False`, try running `man test` or have a look 
[here]( https://tldp.org/LDP/abs/html/fto.html ). 
Specifically, here the `-r` flag tests if you are allowed to read the file or 
not.

Some further useful settings that you could consider adding to your 
`.bashrc`:

```sh
# stage deleted files
alias git_adel="git status -s | grep -E '^ D' | cut -d ' ' -f3 | xargs git add --all"

# git init with standard gitignore
git_init ()
{
    # initialize a git repo
    git init
    # copy standard .gitignore
    cp $HOME/.gitignore ./.gitignore
    if [ $? -eq 0 ]; then
        printf "\n### Copied ~/.gitignore to local directory\n"
    else
        printf "\n### Failed to copy ~/.gitignore\n"
    fi
}
```

_Explanation:_ 
please read the manual entry for `alias`, but essentially it simply maps the 
RHS to the LHS. 
The git_init function initializes a git directory and copies a default 
`.gitgnore` file from your `home` to the current directory. 
A `.gitignore` file tells git which files should be ignored. 
For example you could place the [this](./resources/example_gitignore) files 
in your home directory.

## Home away from home

Depending on the HPC set-up, your allocated home directory size may be
tiny and insufficient to install anything on.
In these cases it is often standard practice to create a secondary 
home directory in a group/project directory (which will need 
to be set-up by the HPC admin). 
Be sure to add this "home-away-from-home" in you `.bashrc` and 
to the `PATH`. 

For example this is in one of my cluster set-up's

```sh
GROUP_HOME='/hpc/dhl_ec/afschmidt'
export PATH="${GROUP_HOME}:${PATH}"
```

These two lines 1) create the environmental variable `GROUP_HOME`, and 2) add
it to the _front_ of your existing path. 

Try running `echo $GROUP_HOME` and `echo $PATH`. The PATH variable is simply a
colon separated list of absolute paths, which bash will search for 
executable programs you may have saved to these locations. 
Essentially is spares you from having the write the entire path to the 
function.

### Sharing resources

It may seem beneficial to allow other people to access the programs you might 
want to install in your `$GROUP_HOME` or even create a specific directory
of programs used by multiple people. 

Unless there is a single person willing to organize and maintain these programs - ensuring multiple version of a program can co-exists, it is almost always
better to have each user install and maintain whatever software (and versions)
they require themselves.

## Installing programs locally 

Most HPC will have many programs pre-installed and universally come the 
full repertoire of UNIX programs such as `sed`, `cut`, `awk` and many 
more. 
Often times using a combination of these programs will get the job done, 
and, depending on your code, will likely be portable to most other 
clusters/environments. 

Nevertheless you will want to install additional software, either in your 
`$HOME` directory or in your `$GROUP_HOME`. 
As an example we will go over installing a (newer) version of vim, followed
by installing conda. 
As a bonus we will also install `peep`, a viewer for tabular data by 
*Chris Finan*.

### vim 

For those unaware vim is simply nano, only better, and if you put in the time 
you will never use a different text editor (possibly Emacs, if you are less
rugged). 

Instead of directly updating the pre-install version of vim, we will create a
new directory mkdir ~/vim && cd !$ to clone the vim installation files to.
Next create a directory where vim will be installed mkdir ~/usr/vim.
Now, let's clone and install vim:

```bash
git clone https://github.com/vim/vim.git
# move to the src directory
cd src
# if this is an actual update clean the previous install
make distclean
# view the possible configurations ./configure --help
# here we will use
./configure --enable-python3interp=dynamic --prefix=$HOME/usr/vim --with-features=huge --with-x
# installing
make
make install

# add vim alias and path 
vim ~/.bashrc
alias vi='vim'
alias vim="${HOME}/usr/vim/bin/vim"
export PATH="${HOME}/usr/vim/bin:${PATH}"
```

Note, this already provides a first glimpse of the benefit of `git`.
Have a look [here](https://gitlab.com/SchmidtAF/vim_tutorial) if you want to 
learn vim for yourself. 
If you do not actually want to use vim for yourself, and we know each other, 
please download the following `.vimrc` file to your home directory:

```sh
cd ~
wget https://gitlab.com/SchmidtAF/vim_tutorial/-/blob/master/.minimal_vimrc
```


This contains all my favourite vim configurations! Yes, you will find that 
`rc` is often used to indication a programs file configurations. 

### conda

Conda is a package manager for `python` and `R` that allows the user to 
specify, share and maintain software environments, allowing a multitude 
of distinct versions to mutually co-exist. 
Install conda as follows: 

```sh
cd ~
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

Next we can install two general purpose environments, one for python, and a
second one for R.
First have a look in `./resources` and run through the two `.yml` files. 
These can be used to install the `renv` and `pyenv` environment by typing:

```sh
conda env create -f ./resources/python.yml
conda env create -f ./resources/renv.yml
```

These will likely take some time to finish. 
Afterwards an environment can be activated by typing 
`conda activate renv` or `conda activate pyenv`. 
After activating an environment you can start R or python, by simply typing
`R` or `python`.
Additional, package can be either added to the `yml` file, and used to 
update the environment with. 
Or, after you have activate your environment, run `conda install PACKAGE_NAME`;
see [here](https://anaconda.org/conda-forge/r-ggplot2)

For further details have a look [here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html). 

### peep 

> peep is more than fantastic - A F Schmidt 

Peep is a python wrapper of less -S, to view tabular data on the command line.
It massively improves your command line life.

Please install as follows:

```sh 
cd ~
git clone git@gitlab.com:cfinan/pyaddons.git
cd pyaddons
# activate your conda environment (here we will use base)
conda activate base
# install dependencies
conda env update --file conda_env/pyaddons_conda_update.yml
# install package
pip install -e .

# add path to path
vim ~/.bashrc
export PATH="${HOME}/pyaddons/resources/bin:${PATH}"
```

Now you can view a tabular file by typing `peep FILENAME`; please also read
`peep -h`.





